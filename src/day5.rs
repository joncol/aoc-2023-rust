// Day 5: If You Give A Seed A Fertilizer

use std::{
    cmp::{max, min},
    fs,
};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
struct Interval {
    start: i64,
    length: i64,
    transformed: bool,
}

impl Interval {
    fn new(start: i64, length: i64, transformed: bool) -> Self {
        Self {
            start,
            length,
            transformed,
        }
    }

    fn apply_range_transform(&self, range_transform: &RangeTransform) -> Vec<Interval> {
        let dx = range_transform.dest - range_transform.source;
        if range_transform.source >= self.start + self.length
            || range_transform.source + range_transform.length - 1 < self.start
        {
            // No intersection.
            vec![*self]
        } else if range_transform.source <= self.start
            && self.start + self.length - 1 < range_transform.source + range_transform.length
        {
            // Interval is fully contained within range transform source segment.
            vec![Interval::new(self.start + dx, self.length, true)]
        } else {
            // Interval part that is transformed.
            let mapped_start = max(self.start, range_transform.source);
            let mapped_end = min(
                self.start + self.length,
                range_transform.source + range_transform.length,
            );
            let mapped_length = mapped_end - mapped_start;
            let mut result = vec![Interval::new(mapped_start + dx, mapped_length, true)];
            // Add interval part to the left of range transform start, if any.
            if self.start < range_transform.source {
                result.push(Interval::new(
                    self.start,
                    range_transform.source - self.start,
                    false,
                ));
            }
            // Add interval part to the right of range transform end, if any.
            let rt_source_end = range_transform.source + range_transform.length;
            if self.start + self.length > rt_source_end {
                result.push(Interval::new(
                    rt_source_end,
                    self.start + self.length - rt_source_end,
                    false,
                ));
            }
            result
        }
    }

    fn apply_interval_map(&self, interval_map: &IntervalMap) -> Vec<Interval> {
        let result = interval_map
            .iter()
            .fold(vec![*self], |intervals, range_transform| {
                intervals
                    .iter()
                    .flat_map(|&i| {
                        if !i.transformed {
                            i.apply_range_transform(range_transform)
                        } else {
                            vec![i]
                        }
                    })
                    .collect()
            });
        if result.is_empty() {
            vec![*self]
        } else {
            result
        }
    }
}

type IntervalMap = Vec<RangeTransform>;

#[derive(PartialEq, Eq, Debug)]
struct RangeTransform {
    dest: i64,
    source: i64,
    length: i64,
}

impl RangeTransform {
    fn parse(s: &str) -> RangeTransform {
        let ns = s
            .split_ascii_whitespace()
            .map(|s| s.parse::<i64>().unwrap())
            .collect::<Vec<i64>>();
        RangeTransform {
            dest: ns[0],
            source: ns[1],
            length: ns[2],
        }
    }

    fn project(&self, x: i64) -> Option<i64> {
        if self.source <= x && x < self.source + self.length {
            Some(x - self.source + self.dest)
        } else {
            None
        }
    }
}

pub fn read_paragraphs(path: &str) -> Vec<String> {
    fs::read_to_string(path)
        .unwrap()
        .split("\n\n")
        .map(|p| p.to_string())
        .collect()
}

pub fn solve_part1() -> i64 {
    let (seeds, maps) = read_seeds_and_maps("data/day5.txt");
    seeds
        .into_iter()
        .map(|s| {
            // For each seed, step through all the maps, and find projections.
            maps.iter().fold(s, |n, m| {
                // For each map, go through all its ranges, and check if it
                // applies.
                m.iter()
                    .map(|interval_map| interval_map.project(n))
                    .reduce(Option::or)
                    .flatten()
                    .unwrap_or(n)
            })
        })
        .min()
        .unwrap()
}

pub fn solve_part2() -> i64 {
    // 69841803 is the right answer.
    let (seeds, maps) = read_seeds_and_maps("data/day5.txt");
    seeds
        .chunks(2)
        .flat_map(|seed_range| {
            let interval = Interval::new(seed_range[0], seed_range[1], false);
            maps.iter().fold(vec![interval], |intervals, interval_map| {
                intervals
                    .iter()
                    .flat_map(|&i| {
                        (Interval {
                            transformed: false,
                            ..i
                        })
                        .apply_interval_map(interval_map)
                    })
                    .collect()
            })
        })
        .map(|i: Interval| i.start)
        .min()
        .unwrap()
}

fn read_seeds_and_maps(filename: &str) -> (Vec<i64>, Vec<IntervalMap>) {
    let paragraphs = read_paragraphs(filename);
    let seeds = paragraphs[0]
        .split(':')
        .nth(1)
        .unwrap()
        .split_ascii_whitespace()
        .map(|s| s.parse().unwrap())
        .collect::<Vec<i64>>();
    let maps = paragraphs
        .iter()
        .skip(1)
        .map(|p| p.lines().skip(1).map(RangeTransform::parse).collect())
        .collect::<Vec<IntervalMap>>();
    (seeds, maps)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_interval_map_test() {
        assert_eq!(
            RangeTransform::parse("50 98 2"),
            RangeTransform {
                dest: 50,
                source: 98,
                length: 2,
            }
        );
    }

    #[test]
    fn project_interval_map_test() {
        let rt = RangeTransform {
            dest: 50,
            source: 98,
            length: 2,
        };
        assert_eq!(rt.project(97), None);
        assert_eq!(rt.project(98), Some(50));
        assert_eq!(rt.project(99), Some(51));
        assert_eq!(rt.project(100), None);
    }

    #[test]
    fn apply_range_transform_test1() {
        let i = Interval::new(79, 14, false);
        let rt = RangeTransform {
            dest: 50,
            source: 98,
            length: 2,
        };
        assert_eq!(
            i.apply_range_transform(&rt),
            vec![Interval::new(79, 14, false)]
        );
    }

    #[test]
    fn apply_range_transform_test2() {
        let i = Interval::new(0, 1, false);
        let rt = RangeTransform {
            dest: 0,
            source: 1,
            length: 1,
        };
        assert_eq!(
            i.apply_range_transform(&rt),
            vec![Interval::new(0, 1, false)]
        );
    }

    #[test]
    fn apply_range_transform_test3() {
        let i = Interval::new(1, 1, false);
        let rt = RangeTransform {
            dest: 10,
            source: 0,
            length: 1,
        };
        assert_eq!(
            i.apply_range_transform(&rt),
            vec![Interval::new(1, 1, false)]
        );
    }

    #[test]
    fn apply_range_transform_test4() {
        // Interval fully within IntervalMap.
        let i = Interval::new(4, 6, false);
        let rt = RangeTransform {
            dest: 0,
            source: 2,
            length: 10,
        };
        assert_eq!(
            i.apply_range_transform(&rt),
            vec![Interval::new(2, 6, true)]
        );
    }

    #[test]
    fn apply_range_transform_test5() {
        // IntervalMap fully within Interval.
        let i = Interval::new(10, 10, false);
        let rt = RangeTransform {
            dest: 0,
            source: 12,
            length: 6,
        };
        assert_eq!(
            i.apply_range_transform(&rt),
            vec![
                Interval::new(0, 6, true),
                Interval::new(10, 2, false),
                Interval::new(18, 2, false)
            ]
        );
    }

    #[test]
    fn apply_range_transform_test6() {
        let i = Interval::new(10, 10, false);
        let rt = RangeTransform {
            dest: 0,
            source: 8,
            length: 6,
        };
        assert_eq!(
            i.apply_range_transform(&rt),
            vec![Interval::new(2, 4, true), Interval::new(14, 6, false),]
        );
    }

    #[test]
    fn apply_range_transform_test7() {
        let i = Interval::new(10, 10, false);
        let rt = RangeTransform {
            dest: 0,
            source: 16,
            length: 6,
        };
        assert_eq!(
            i.apply_range_transform(&rt),
            vec![Interval::new(0, 4, true), Interval::new(10, 6, false),]
        );
    }

    #[test]
    fn apply_range_transform_test8() {
        let i = Interval::new(10, 10, false);
        let rt = RangeTransform {
            dest: 0,
            source: 10,
            length: 6,
        };
        assert_eq!(
            i.apply_range_transform(&rt),
            vec![Interval::new(0, 6, true), Interval::new(16, 4, false),]
        );
    }

    #[test]
    fn apply_range_transform_test9() {
        let i = Interval::new(10, 10, false);
        let rt = RangeTransform {
            dest: 0,
            source: 14,
            length: 6,
        };
        assert_eq!(
            i.apply_range_transform(&rt),
            vec![Interval::new(0, 6, true), Interval::new(10, 4, false),]
        );
    }

    #[test]
    fn apply_range_transform_test10() {
        // Self-overlapping.
        let i = Interval::new(10, 10, false);
        let rt = RangeTransform {
            dest: 12,
            source: 14,
            length: 4,
        };
        assert_eq!(
            i.apply_range_transform(&rt),
            vec![
                Interval::new(12, 4, true),
                Interval::new(10, 4, false),
                Interval::new(18, 2, false),
            ]
        );
    }

    #[test]
    fn apply_range_transform_test11() {
        // Self-overlapping.
        let i = Interval::new(10, 10, false);
        let rt = RangeTransform {
            dest: 14,
            source: 12,
            length: 4,
        };
        assert_eq!(
            i.apply_range_transform(&rt),
            vec![
                Interval::new(14, 4, true),
                Interval::new(10, 2, false),
                Interval::new(16, 4, false),
            ]
        );
    }

    #[test]
    fn apply_range_transform_test12() {
        let i = Interval::new(55, 13, false);
        let rt = RangeTransform {
            dest: 52,
            source: 50,
            length: 48,
        };
        assert_eq!(
            i.apply_range_transform(&rt),
            vec![Interval::new(57, 13, true),]
        );
    }

    #[test]
    fn apply_range_transform_test13() {
        let i = Interval::new(74, 14, false);
        let rt = RangeTransform {
            dest: 68,
            source: 64,
            length: 13,
        };
        assert_eq!(
            i.apply_range_transform(&rt),
            vec![Interval::new(78, 3, true), Interval::new(77, 11, false)]
        );
    }

    #[test]
    fn apply_interval_map1() {
        let i = Interval::new(79, 14, false);
        let im = vec![
            RangeTransform {
                dest: 50,
                source: 98,
                length: 2,
            },
            RangeTransform {
                dest: 52,
                source: 50,
                length: 48,
            },
        ];
        assert_eq!(i.apply_interval_map(&im), vec![Interval::new(81, 14, true)]);
    }

    #[test]
    fn apply_interval_map2() {
        let i = Interval::new(74, 14, false);
        let im = vec![
            RangeTransform {
                dest: 45,
                source: 77,
                length: 23,
            },
            RangeTransform {
                dest: 68,
                source: 64,
                length: 13,
            },
        ];
        assert_eq!(
            i.apply_interval_map(&im),
            vec![Interval::new(45, 11, true), Interval::new(78, 3, true)]
        );
    }
}
