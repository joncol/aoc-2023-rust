// Day 2: Cube Conundrum

use aoc_2023::util::read_lines;

#[derive(Debug, PartialEq, Eq)]
struct Game {
    id: i32,
    rounds: Vec<Round>,
}

impl Game {
    fn parse(s: &str) -> Game {
        let parts: Vec<&str> = s.split(": ").collect();
        let id = parts[0].split_ascii_whitespace().nth(1).unwrap().parse::<i32>().unwrap();
        let rounds = parts[1]
            .split("; ")
            .map(Round::parse)
            .collect::<Vec<Round>>();
        Game { id, rounds }
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Round {
    red: i32,
    green: i32,
    blue: i32,
}

impl Round {
    fn parse(s: &str) -> Round {
        let s = s.replace(',', "");
        let parts: Vec<&str> = s.split_ascii_whitespace().collect();
        let mut it = parts.iter();
        let mut red = 0;
        let mut green = 0;
        let mut blue = 0;
        while let Some(n) = it.next() {
            let n = n.parse::<i32>().unwrap();
            let color = *it.next().unwrap();
            match color {
                "red" => {
                    red = n;
                }
                "green" => {
                    green = n;
                }
                "blue" => {
                    blue = n;
                }
                _ => {
                    unreachable!();
                }
            }
        }
        Round { red, green, blue }
    }
}

pub fn solve_part1() -> i32 {
    let lines = read_lines("data/day2.txt");
    lines
        .iter()
        .map(|s| Game::parse(&s[..]))
        .filter(|g| {
            g.rounds
                .iter()
                .all(|r| r.red <= 12 && r.green <= 13 && r.blue <= 14)
        })
        .map(|g| g.id)
        .sum()
}

pub fn solve_part2() -> i32 {
    let lines = read_lines("data/day2.txt");
    lines
        .iter()
        .map(|s| Game::parse(&s[..]))
        .map(|g| {
            g.rounds.iter().map(|r| r.red).max().unwrap()
                * g.rounds.iter().map(|r| r.green).max().unwrap()
                * g.rounds.iter().map(|r| r.blue).max().unwrap()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_round_test() {
        assert_eq!(
            Round::parse("3 blue, 4 red"),
            Round {
                red: 4,
                green: 0,
                blue: 3
            }
        );
    }

    #[test]
    fn parse_game_test() {
        assert_eq!(
            Game::parse("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"),
            Game {
                id: 1,
                rounds: vec![
                    Round {
                        red: 4,
                        green: 0,
                        blue: 3,
                    },
                    Round {
                        red: 1,
                        green: 2,
                        blue: 6,
                    },
                    Round {
                        red: 0,
                        green: 2,
                        blue: 0,
                    }
                ]
            }
        );
    }
}
