// Day 3: Gear Ratios

use std::collections::HashSet;

use aoc_2023::util::read_lines;

#[derive(PartialEq, Eq, Hash, Debug)]
struct Part {
    x1: i32,
    x2: i32,
    y: i32,
    number: i32,
}

#[derive(PartialEq, Eq, Hash, Debug)]
struct Symbol {
    x: i32,
    y: i32,
    symbol: char,
}

fn parse_schematic(filename: &str) -> (HashSet<Part>, HashSet<Symbol>) {
    let lines = read_lines(filename);
    let mut parts = HashSet::<Part>::new();
    let mut symbols = HashSet::<Symbol>::new();
    for (y, line) in lines.iter().enumerate() {
        let line = format!("{}.", line); // Hack to fix numbers at end of line.
        let mut num_str = String::new();
        for (x, ch) in line.chars().enumerate() {
            let x = x as i32;
            if ch.is_ascii_digit() {
                num_str.push(ch);
            } else {
                if ch != '.' {
                    symbols.insert(Symbol {
                        x,
                        y: y as i32,
                        symbol: ch,
                    });
                }

                if !num_str.is_empty() {
                    let number = num_str.parse::<i32>().unwrap();
                    parts.insert(Part {
                        x1: x - num_str.len() as i32,
                        x2: x - 1,
                        y: y as i32,
                        number,
                    });
                    num_str.truncate(0);
                }
            }
        }
    }
    (parts, symbols)
}

pub fn solve_part1() -> i32 {
    let (parts, symbols) = parse_schematic("data/day3.txt");
    parts
        .iter()
        .filter(|part| symbols.iter().any(|symbol| is_adjacent(part, symbol)))
        .map(|part| part.number)
        .sum()
}

pub fn solve_part2() -> i32 {
    let (parts, symbols) = parse_schematic("data/day3.txt");
    symbols
        .iter()
        .filter(|symbol| {
            symbol.symbol == '*'
                && parts
                    .iter()
                    .filter(|part| is_adjacent(part, symbol))
                    .count()
                    == 2
        })
        .map(|symbol| {
            parts
                .iter()
                .filter(|part| is_adjacent(part, symbol))
                .map(|part| part.number)
                .product::<i32>()
        })
        .sum()
}

fn is_adjacent(part: &Part, symbol: &Symbol) -> bool {
    i32::abs(symbol.y - part.y) <= 1 && part.x1 - 1 <= symbol.x && symbol.x <= part.x2 + 1
}
