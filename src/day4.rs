// Day 4: Scratchcards

use std::{cmp::min, collections::HashSet, iter::FromIterator};

use aoc_2023::util::read_lines;

#[derive(PartialEq, Eq, Hash, Debug)]
struct Card {
    id: i32,
    winning_numbers: Vec<i32>,
    numbers: Vec<i32>,
}

impl Card {
    fn parse(s: &str) -> Card {
        let parts = s.split(" | ").collect::<Vec<&str>>();
        let left_parts = parts[0].split(':').collect::<Vec<&str>>();
        let id_str = left_parts[0].replace("Card", "");
        let id_str = id_str.trim();
        let id = id_str.parse().unwrap();
        let winning_numbers = left_parts[1]
            .split_ascii_whitespace()
            .map(|s| s.parse::<i32>().unwrap())
            .collect();
        let numbers = parts[1]
            .split_ascii_whitespace()
            .map(|s| s.parse().unwrap())
            .collect();
        Card {
            id,
            winning_numbers,
            numbers,
        }
    }
}

pub fn solve_part1() -> i32 {
    let lines = read_lines("data/day4.txt");
    lines
        .iter()
        .map(|line| Card::parse(line))
        .map(|card| matching_number_count(&card))
        .map(|n| if n > 0 { i32::pow(2, n as u32 - 1) } else { 0 })
        .sum()
}

pub fn solve_part2() -> i32 {
    let lines = read_lines("data/day4.txt");
    let cards = lines.iter().map(|line| Card::parse(line));
    let match_counts = cards
        .map(|card| matching_number_count(&card))
        .collect::<Vec<i32>>();
    // Start with one copy of each card.
    let mut card_counts = vec![1; lines.len()];
    for i in 0..lines.len() {
        for j in 1..=min(match_counts[i] as usize, lines.len() - i - 1) {
            card_counts[i + j] += card_counts[i];
        }
    }
    card_counts.iter().sum()
}

fn matching_number_count(card: &Card) -> i32 {
    let winning_numbers: HashSet<i32> = HashSet::from_iter(card.winning_numbers.iter().cloned());
    let numbers: HashSet<i32> = HashSet::from_iter(card.numbers.iter().cloned());
    winning_numbers.intersection(&numbers).count() as i32
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_card_test() {
        let card_str = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53";
        assert_eq!(
            Card::parse(card_str),
            Card {
                id: 1,
                winning_numbers: vec![41, 48, 83, 86, 17],
                numbers: vec![83, 86, 6, 31, 17, 9, 48, 53]
            }
        );
    }
}
