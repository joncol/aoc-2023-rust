use std::fs;

pub fn read_lines(path: &str) -> Vec<String> {
    fs::read_to_string(path)
        .unwrap()
        .lines()
        .map(|l| l.to_string())
        .collect()
}
