mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;

fn main() {
    println!("Day 1, part 1: {}", day1::solve_part1());
    println!("Day 1, part 2: {}", day1::solve_part2());
    println!("Day 2, part 1: {}", day2::solve_part1());
    println!("Day 2, part 2: {}", day2::solve_part2());
    println!("Day 3, part 1: {}", day3::solve_part1());
    println!("Day 3, part 2: {}", day3::solve_part2());
    println!("Day 4, part 1: {}", day4::solve_part1());
    println!("Day 4, part 2: {}", day4::solve_part2());
    println!("Day 5, part 1: {}", day5::solve_part1());
    println!("Day 5, part 2: {}", day5::solve_part2());
    println!("Day 6, part 1: {}", day6::solve_part1());
    // println!("Day 6, part 2: {}", day6::solve_part2());
}
