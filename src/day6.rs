use aoc_2023::util::read_lines;

pub fn solve_part1() -> i32 {
    let lines = read_lines("data/day6.txt");
    let times = parse_line(&lines[0]);
    let dists = parse_line(&lines[1]);
    times
        .iter()
        .zip(dists.iter())
        .map(|(&t, &dist)| {
            (1..t)
                .map(move |speed| distance(speed, t - speed))
                .filter(move |&d| d > dist)
                .count() as i32
        })
        .product()
}

fn parse_line(s: &str) -> Vec<i32> {
    s.split_ascii_whitespace()
        .skip(1)
        .map(|s| s.parse::<i32>().unwrap())
        .collect()
}

fn distance(speed: i32, travel_time: i32) -> i32 {
    speed * travel_time
}
