// Day 1: Trebuchet?!

use aoc_2023::util::read_lines;

use std::collections::HashMap;

fn keep_digits(s: String) -> String {
    s.chars().filter(|c| c.is_ascii_digit()).collect()
}

fn first_and_last(s: String) -> String {
    format!(
        "{}{}",
        s.chars().next().unwrap(),
        s.chars().nth(s.len() - 1).unwrap()
    )
}

fn first_and_last_digit_strings(s: String) -> String {
    let digits = HashMap::from([
        ("one", '1'),
        ("two", '2'),
        ("three", '3'),
        ("four", '4'),
        ("five", '5'),
        ("six", '6'),
        ("seven", '7'),
        ("eight", '8'),
        ("nine", '9'),
    ]);
    let mut first_digit = None;
    let mut last_digit = None;
    for i in 0..s.len() {
        let ch = s.chars().nth(i).unwrap();
        if ch.is_ascii_digit() {
            if first_digit.is_none() {
                first_digit = Some(ch);
            }
            last_digit = Some(ch);
        } else {
            for (&k, &v) in digits.iter() {
                if i + k.len() <= s.len() && &s[i..i + k.len()] == k {
                    if first_digit.is_none() {
                        first_digit = Some(v);
                    }
                    last_digit = Some(v);
                    break;
                }
            }
        }
    }
    let mut result = String::new();
    result.push(first_digit.unwrap());
    result.push(last_digit.unwrap());
    result
}

pub fn solve_part1() -> i32 {
    let lines = read_lines("data/day1.txt");
    lines
        .into_iter()
        .map(keep_digits)
        .map(first_and_last)
        .map(|s| s.parse::<i32>().unwrap())
        .sum()
}

pub fn solve_part2() -> i32 {
    let lines = read_lines("data/day1.txt");
    lines
        .into_iter()
        .map(first_and_last_digit_strings)
        .map(|s| s.parse::<i32>().unwrap())
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn first_and_last_digit_strings_test1() {
        assert_eq!(first_and_last_digit_strings("two1nine".to_string()), "29");
    }

    #[test]
    fn first_and_last_digit_strings_test2() {
        assert_eq!(first_and_last_digit_strings("eightwo".to_string()), "82");
    }
}
